resource "helm_release" "efsdriverplugin" {
  count = var.create_efsdriverplugin ? 1 : 0

  name       = "awsefscsidriver"
  chart      = "https://github.com/kubernetes-sigs/aws-efs-csi-driver/releases/download/helm-chart-aws-efs-csi-driver-2.2.6/aws-efs-csi-driver-2.2.6.tgz"
  namespace  = "kube-system"

  set {
    name  = "image.repository"
    value = "602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/aws-efs-csi-driver"
  }

  # Since we used managed Kubernetes, we don't need to create on the controller
  set {
    name  = "controller.create"
    value = "false"
  }

  set {
    name  = "controller.serviceAccount.create"
    value = "false"
  }

  set {
    name  = "controller.serviceAccount.name"
    value = "efs-csi-controller-sa"
  }

  set {
    name  = "node.serviceAccount.create"
    value = "true"
  }

  set {
    name  = "controller.serviceAccount.name"
    value = "efs-csi-node-sa"
  }

  set {
    name  = "replicaCount"
    value = "${var.efsdriverplugin_replicas}"
  }
}
